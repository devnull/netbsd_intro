# Découverte de NetBSD 7.0

Document LaTeX de présentation du système NetBSD 7.0 dans le cadre d'une [LP ASRALL (Licence Pro Administration Systèmes, Réseaux et Applications à base de Logiciels Libres)](http://iut-charlemagne.univ-lorraine.fr/content/licence-professionnelle-administration-de-systemes-reseaux-et-applications-base-de-logiciels).

Il est possible de compiler ce document avec la commande make (GNU make ou BSD-Make) si le paquet latex-make \[1] est installé

[1] Soit texlive-latex-make pour les systèmes de la famille Fedora